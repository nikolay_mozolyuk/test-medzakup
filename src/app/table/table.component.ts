import {
  ApellationType,
  ApellationTypeService,
} from './../services/apellation-type.service';
import { Component, Input, ViewChild } from '@angular/core';
import * as FileSaver from 'file-saver';

interface Column {
  field: string;
  header: string;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent {
  apellationType!: ApellationType;

  apellationTypeDialog: boolean = false;

  hideColumn: boolean = false;

  _selectedColumns!: Column[];

  @ViewChild('dt') dt: any;

  submitted: boolean = false;

  cols!: Column[];

  typeKeys = [
    'mnn_id',
    'subtype',
    'num',
    'name',
    'release_form',
    'dosage',
    'unit',
  ];

  constructor(public apellationTypeService: ApellationTypeService) {
    apellationTypeService.load();
  }

  ngOnInit() {
    this.cols = [
      { field: 'mnn_id', header: 'Ідентифікатор МНН' },
      { field: 'subtype', header: 'Піднапрям' },
      { field: 'num', header: '№ позиції номенклатури' },
      { field: 'name', header: 'МНН' },
      { field: 'release_form', header: 'Форма випуску' },
      { field: 'dosage', header: 'Дозування' },
      { field: 'unit', header: 'Одиниці виміру' },
    ];

    this._selectedColumns = this.cols;
  }

  toggleColumnVisibility() {
    this.hideColumn = !this.hideColumn;
  }

  applyFilterGlobal($event: any, stringVal: any) {
    this.dt.filterGlobal(($event.target as HTMLInputElement).value, stringVal);
  }

  exportExcel(): void {
    import('xlsx').then((xlsx) => {
      const worksheet = xlsx.utils.json_to_sheet(
        this.apellationTypeService.apellationTypes
      );
      const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: 'xlsx',
        type: 'array',
      });
      this.saveAsExcelFile(excelBuffer, 'products');
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE =
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    FileSaver.saveAs(
      data,
      fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION
    );
  }

  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }

  set selectedColumns(val: any[]) {
    this._selectedColumns = this.cols.filter((col) => val.includes(col));
  }

  openNew() {
    this.apellationType = {
      subtype: '',
      num: null,
      name: '',
      release_form: '',
      dosage: '',
      unit: '',
    };

    this.submitted = false;
    this.apellationTypeDialog = true;
  }

  hideDialog() {
    this.apellationTypeDialog = false;
    this.submitted = false;
  }

  save() {
    this.submitted = true;
    this.apellationTypeService.sendData(this.apellationType)
    
  }
}
