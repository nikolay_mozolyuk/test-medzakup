import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TableComponent } from './table.component';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TableComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize with correct columns', () => {
    const expectedColumns = [
      { field: 'mnn_id', header: 'Ідентифікатор МНН' },
      { field: 'subtype', header: 'Піднапрям' },
      { field: 'num', header: '№ позиції номенклатури' },
      { field: 'name', header: 'МНН' },
      { field: 'release_form', header: 'Форма випуску' },
      { field: 'dosage', header: 'Дозування' },
      { field: 'unit', header: 'Одиниці виміру' },
    ];

    expect(component.cols).toEqual(expectedColumns);
    expect(component._selectedColumns).toEqual(expectedColumns);
  });

  it('should toggle column visibility', () => {
    expect(component.hideColumn).toBeFalse();

    component.toggleColumnVisibility();
    expect(component.hideColumn).toBeTrue();

    component.toggleColumnVisibility();
    expect(component.hideColumn).toBeFalse();
  });
});
