import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

export interface ApellationType {
  mnn_id?: number | null;
  subtype: string;
  num: number | null;
  name: string;
  release_form: string;
  dosage: string;
  unit: string;
}

@Injectable({
  providedIn: 'root',
})
export class ApellationTypeService {
  baseUrl = 'https://api.medzakupivli.com/';
  apellationTypes: ApellationType[] = [];

  constructor(private readonly httpClient: HttpClient) {
    this.load();
  }

  load(): void {
    this.httpClient
      .get(
        this.baseUrl +
          'appellation/type/?hash=8f7d225ffda84d9a143ca8c9868779a95cc9b033'
      )
      .subscribe((resp: any) => {
        this.apellationTypes = resp;
      });
  }

  getApellationTypes(): ApellationType[] {
    return this.apellationTypes;
  }

  sendData(apellationType: ApellationType): void {
    this.httpClient
      .post(this.baseUrl + 'inbound_logistics/angular/?name=mozoliuk', apellationType)
      .subscribe((resp) => {
        console.log(resp);
      });
  }
}
